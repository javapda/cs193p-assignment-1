//
//  CalculatorBrain.m
//  Calculator
//
//  Created by John Kroubalkian on 6/25/12.
//  Copyright (c) 2012 Jed Clampett Dining and Fishing, Inc. All rights reserved.
// added a comment, again, yet another, one more time

#import "CalculatorBrain.h"

@interface CalculatorBrain()
@property (nonatomic,strong) NSMutableArray *operandStack;
@end


@implementation CalculatorBrain
@synthesize operandStack = _operandStack;
-(NSString *)dumpStack
{
    return [self.operandStack description];
}
-(NSMutableArray*) operandStack {
    if(_operandStack==nil) {
        _operandStack = [[NSMutableArray alloc]init];
    }
    return _operandStack;
}
-(void)clearOperands
{
    NSLog(@"START:clearOperations %d",self.operandStack.count);
          [self.operandStack removeAllObjects];
    NSLog(@"END:clearOperations %d",self.operandStack.count);
}
-(void)pushOperand:(double)operand
{
    [self.operandStack addObject:[NSNumber numberWithDouble:operand]];
    
}
-(double)popOperand 
{
    NSNumber *operandObject = [self.operandStack lastObject];
    if(operandObject) {
        [self.operandStack removeLastObject];
    }
    return [operandObject doubleValue];
}
-(double)performOperation:(NSString *)operation;
{
//    NSLog(@"Stack size: %d",self.operandStack.count);
    NSLog(@"%@ operation",operation);        
    double result = 0;
    // calculate result
    if ([operation isEqualToString:@"+"]) {
        result = [self popOperand] + [self popOperand];
    } else if ([operation isEqualToString:@"-"]) {
        result = [self popOperand] - [self popOperand];
    } else if ([operation isEqualToString:@"*"]) {
        result = [self popOperand] * [self popOperand];
    } else if ([operation isEqualToString:@"/"]) {
        result = [self popOperand] / [self popOperand];
    } else if ([operation isEqualToString:@"sqrt"]) {
        result = sqrt([self popOperand]);
    } else if ([operation isEqualToString:@"x^2"]) {
        result = [self popOperand];
        result *= result;
    } else if ([operation isEqualToString:@"sin"]) {
        result = [self popOperand];
        result *= (M_PI)/180;
        if (result>0) {
            result=sin(result);
        }
    } else if ([operation isEqualToString:@"cos"]) {
        result = [self popOperand];
        result *= (M_PI)/180;
        if (result>=0) {
            result=cos(result);
        }
    } else if ([operation isEqualToString:@"tan"]) {
        result = [self popOperand];
        result *= (M_PI)/180;
        if (result>=0) {
            result=tan(result);
        }
    } else {
        NSLog(@"UNKNOWN operation %@",operation);        
    }
    [self pushOperand:result];
    return result;
}

@end
