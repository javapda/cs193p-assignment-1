//
//  CalculatorBrain.h
//  Calculator
//
//  Created by John Kroubalkian on 6/25/12.
//  Copyright (c) 2012 Jed Clampett Dining and Fishing, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalculatorBrain : NSObject
-(void)pushOperand:(double)operand;
-(double)popOperand;
-(double)performOperation:(NSString *)operation;
-(void)clearOperands;
-(NSString *)dumpStack;
@end
