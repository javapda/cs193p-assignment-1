//
//  CalculatorViewController.h
//  Calculator
//
//  Created by John Kroubalkian on 6/24/12.
//  Copyright (c) 2012 Jed Clampett Dining and Fishing, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>  // framework header file

@interface CalculatorViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *display;
@property (weak, nonatomic) IBOutlet UILabel *stackDisplay;
@property (weak, nonatomic) IBOutlet UILabel *brainActivity;

@end
