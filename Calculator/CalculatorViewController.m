//
//  CalculatorViewController.m
//  Calculator
//
//  Created by John Kroubalkian on 6/24/12.
//  Copyright (c) 2012 Jed Clampett Dining and Fishing, Inc. All rights reserved.
//

#import "CalculatorViewController.h"
#import "CalculatorBrain.h"

@interface CalculatorViewController ()
// this is where your private methods go!
@property (nonatomic) BOOL userIsInTheMiddleOfEnteringANumber;
@property (nonatomic,strong) CalculatorBrain *brain;
-(void)updateStackDisplay;
@end

@implementation CalculatorViewController
-(void)updateStackDisplay
{
    NSLog(@"Enter Pressed, %@",self.brain.dumpStack);
    self.stackDisplay.text = self.brain.dumpStack;
}
- (IBAction)sinPressed:(UIButton *)sender {
    [self operationPressed:sender];
    if(YES)return;
    // pop and calc
    double num = [self.brain popOperand];
    num *= (M_PI)/180;
    
    NSLog(@"START:sin pressed, num=%g",num);
//    NSLog(@"M_PI_2: %g",M_PI_2);
    if (num>0) {
    // set display
        num=sin(num);
    // push 
        [self.brain pushOperand:num];
        self.display.text=[NSString stringWithFormat:@"%g",num];
        [self updateStackDisplay];
    }
}
- (IBAction)bkspPressed:(UIButton *)sender {
    NSLog(@"bksp pressed");
    if(self.display.text.length>1) {
        self.display.text = [self.display.text substringToIndex:(self.display.text.length-1)];
    }
}
- (IBAction)cosPressed:(UIButton *)sender {
    NSLog(@"cos pressed");
    [self operationPressed:sender];
    if(YES)return;
    // pop and calc
    double num = [self.brain popOperand];
    num *= (M_PI)/180;
    NSLog(@"START:cos pressed, num=%g",num);
    if (num>=0) {
        // set display
        num=cos(num);
        // push 
        [self.brain pushOperand:num];
        self.display.text=[NSString stringWithFormat:@"%g",num];
        [self updateStackDisplay];
        
    }
}
- (IBAction)tanPressed:(UIButton *)sender {
    NSLog(@"tan pressed");
//    [self operationPressed:sender];
//    if(YES)return;
    // pop and calc
    double num = [self.brain popOperand];
    num *= (M_PI)/180;
    NSLog(@"START:tan pressed, num=%g",num);
    if (num>0) {
        // set display
        num=tan(num);
        // push 
        [self.brain pushOperand:num];
        self.display.text=[NSString stringWithFormat:@"%g",num];
        [self updateStackDisplay];
        
    }
}

- (IBAction)sqrtPressed:(UIButton *)sender {
    [self operationPressed:sender];
    if(YES)return;
    NSLog(@"sqrt pressed");
    double num = [self.brain popOperand];
    num=num*num;
    [self.brain pushOperand:num];
    self.display.text=[NSString stringWithFormat:@"%g",num];
    [self updateStackDisplay];
}
- (IBAction)piPressed:(UIButton *)sender {
    NSLog(@"PI pressed");
    
    self.display.text=[NSString stringWithFormat:@"%g",M_PI];
    [self enterPressed];
}
- (IBAction)clearPressed:(UIButton *)sender {
    NSLog(@"Clear pressed");
    [self.brain clearOperands];
    self.userIsInTheMiddleOfEnteringANumber=NO;
    self.display.text = [NSString stringWithFormat:@"0"];
    [self updateStackDisplay];
    self.brainActivity.text=@"";


}
- (IBAction)decimalPressed:(UIButton *)sender {
    // want to append a "." to end if one not already there
    NSLog(@"decimalPressed");
    NSString *val = self.display.text;
    NSRange range = [val rangeOfString:@"."];
    if (range.length>0) {
        NSLog(@"Matching .");
    }else {
        NSLog(@"NO MATCH .");
        self.display.text=[self.display.text stringByAppendingFormat:@"."];
        self.userIsInTheMiddleOfEnteringANumber=YES;
    }
    [self updateStackDisplay];

    //NSLog(@"range: %@,%@",range.length,range.location);
}
- (IBAction)plusMinusPressed:(UIButton *)sender {
    NSLog(@"%@ pressed...",sender.currentTitle);
    if (self.userIsInTheMiddleOfEnteringANumber) {
        
     if ([self.display.text hasPrefix:@"-"]) {
         self.display.text = [self.display.text substringFromIndex:1];
     } else {
         self.display.text = [NSString stringWithFormat:@"-%@",self.display.text];
     }
        
    }
}
@synthesize brain = _brain;
@synthesize display = _display;
@synthesize stackDisplay = _stackDisplay;
@synthesize brainActivity = _brainActivity;
@synthesize userIsInTheMiddleOfEnteringANumber = _userIsInTheMiddleOfEnteringANumber;
- (IBAction)enterPressed 
{
    NSLog(@"Enter Pressed");
    [self.brain pushOperand:[self.display.text doubleValue]];
    self.brainActivity.text=[self.brainActivity.text stringByAppendingFormat:@"%@ ",self.display.text];
    self.userIsInTheMiddleOfEnteringANumber = NO;
    [self updateStackDisplay];
}
- (IBAction)operationPressed:(UIButton *)sender 
{
    
    NSLog(@"Operation Pressed");
    if (self.userIsInTheMiddleOfEnteringANumber) {
        [self enterPressed];
    }

    self.brainActivity.text=[self.brainActivity.text stringByAppendingFormat:@"%@ = ",sender.currentTitle];
    
    double result = [self.brain performOperation:sender.currentTitle];
    NSLog(@"result: %g",result);
    NSString *resultString = [NSString stringWithFormat:@"%g", result];
    self.display.text=resultString;
    [self updateStackDisplay];

}
- (CalculatorBrain *)brain 
{
    if (!_brain) {
        _brain = [[CalculatorBrain alloc] init];
    }
    return _brain;
}
- (IBAction)digitPressed:(UIButton *)sender {
    NSString *digit = sender.currentTitle;
    if(self.userIsInTheMiddleOfEnteringANumber) {
    self.display.text = [self.display.text stringByAppendingString:digit];
    }else{
        self.display.text = digit;
        self.userIsInTheMiddleOfEnteringANumber = YES;
    }
    NSLog(@"digit=%@, sender: %@",digit,sender);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [self setDisplay:nil];
    [self setStackDisplay:nil];
    [self setBrainActivity:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
